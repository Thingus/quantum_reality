def get_colour_proportions(element)
    """
    Returns the percentage colours for a given element
    
    Returns a dictionary of elements and percentages
    """
    if element:
        return {'red':20,
                'blue':40,
                'green':60}